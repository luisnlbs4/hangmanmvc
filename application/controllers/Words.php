<?php
class Words extends CI_Controller
{
	public function __construct()
		{
			parent::__construct();
			$this->load->model('words_model');
		}

	public function index()
	{
		$data['Words'] = $this->words_model->getWords();
		
		$this->load->view("Words/index", $data);
 	}
}
?>