<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Word_model');

		
	}
	 public function index(){
	 	$data['segmento'] = $this->uri->segment(3);
	 	if(!$data['segmento']){
	 		header("Location: categories");
	 		exit();
		}
		else
		{
	 		$data['words'] = $this->Word_model->getWord($data['segmento']);
		}
		$this->load->view("category/index", $data);
	 }

	 public function create()
	 {
	 	$newWordName = $this->input->post('newWordTextBox');
	 	$categoryid= $this ->input->post('categoryId');
		$lastId = $this->Word_model->createWords($newWordName,$categoryid);
		$this->load->helper('url');
		redirect("category/index/".$categoryid);
	 }
	 public function deleteWord()
	 {
	 	 $data['segmento'] = $this->uri->segment(3);
	 	 $categoryid= $this ->input->post('categoryId');
	 	 $WordsId= $this ->input->post('WordsId');
	 	 $this->Word_model->deleteword($WordsId);
	 	 $this->load->helper('url');
		 redirect("category/index/".$categoryid);

	 }
}
?>