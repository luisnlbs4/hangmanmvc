<?php
/**
* 
*/
class Words_model extends CI_model
{
	
	function __construct()
	{
		$this->load->database();
	}

	function getWords($categoryId)
	{
		$query = $this->db->query("SELECT text FROM word WHERE categoryId=?");
		return $query->result_array();
	}

	function createWord($word, $categoryId) 
	{
		$data2 = $this->db->query("INSERT INTO word(categoryId, text) VALUES (?, ?)");

		$stmt = mysqli_stmt_init($conn);
		$success = false;
		
	    if (mysqli_stmt_prepare($stmt, $sql)) 
		{
			/* bind parameters for markers, other options are: 
			   i - integer, d - double, s - string, b - BLOB */
			mysqli_stmt_bind_param($stmt, "is", $categoryId, $word);

			$success = mysqli_stmt_execute($stmt);
			mysqli_stmt_close($stmt);		
		}
		
		return $success;
	}

	public function createCategory($categoryname)
 	{ 
         $data = array('name'=> $categoryname); // pasamos el categoryname a un string
         $this->db->insert('category',$data); // insertamos en la base de datos
         return $this->db->insert_id(); //aqui devolvemos el id generado
 	}

	function redirectToCategories()
	{
		header("Location: categories");
		exit();
	}
}
?>