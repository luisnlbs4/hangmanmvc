<?php
class Word_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}
	public function getWord($Categoriesid)
	{
		$this->db->where('categoryId',$Categoriesid);
		$query=$this->db->get('word');
		if($query->num_rows()>=0) 
		{
			return $query->result_array();
		}
		else
		{ 
			return false;
		}
	}

	public function createWords($Word, $idCategory)
	{
		$data = array ('text'=> $Word, 'categoryId' => $idCategory);
		$this->db->insert('word',$data);
		return $this->db->insert_id();
	}

    public function deleteword($idword)
    {
    	$this->db->delete("word",array('id'=>$idword));
    }



}
?>