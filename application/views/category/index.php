<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">

    <script type="text/javascript"></script>
    <title>El ahorcado - Categoría:</title>
	<meta charset="UTF-8">
   <style type="text/css">
       h1{
        text-align: center;
       }
    </style>
  </head>
  <body>
   <div class="page-header">
    <h1>Registro de Palabras</h1>
    </div>
    <ul>
    <div class="container">
    <table class="table">
    <tr>
         <th>Palabra</th>
         <th>Opcion Eliminar</th>
    </tr>
	<?php foreach ($words as $word): ?>
    <form method="POST" action="/category/deleteWord">
    <input type="hidden" name="categoryId" value="<?=$segmento?>"/>
    <input type="hidden" name="WordsId" value="<?=$word["id"]?>"/>
    <tr>
	   <td><li><?= $word["text"] ?></li></td>
     <td><input  class="btn btn-danger" type="submit" name="deleteWord" value="eliminar"/></td>
     </tr>
    </form>
	  <?php endforeach; ?>
    
      <form method="POST" action="/category/create">
      <input type="hidden" name="categoryId" value="<?=$segmento?>"/>
      <input type="text" name="newWordTextBox" maxlength="50" />
      <input class="btn btn-success" type="submit" name="createNewWordButton" value="Crear" />
    </form>
    <form method="POST" action="/categories">
       <input class="btn btn-primary" type="submit" name="volver" value="Volver" />
    </form>
    </table>
    </div>
    </ul>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </body>
</html>