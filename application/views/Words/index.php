<html lang="es-BO">
  <head>
    <title>El ahorcado - Categoría: <?= $categoryName ?></title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    <h1>Palabras para Categoría "<?= $categoryName ?>"</h1>
	<ul>
	<?php 
	    if (mysqli_num_rows($result) > 0) {
      
			while($row = mysqli_fetch_array($result))
				{ 
    ?>
			<li><?= $row["text"] ?> [<a href="deleteWord.php?id=<?= $row["id"] ?>" onclick="return confirm('¿Está seguro que desea eliminar esta palabra?');">X</a>]</li>
    <?php }
        }
		
		mysqli_close($conn);
	 ?>
	  <li>
		<form method="POST" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>?<?= $_SERVER["QUERY_STRING"] ?>">
		  <input type="text" name="newWordTextBox" maxlength="50" />
		  <input type="submit" name="createNewWordButton" value="Crear" />
		</form>
	  </li>
	</ul>
	
	<div>
	   <a href="deleteCategory.php?categoryId=<?= $categoryId ?>" onclick="return confirm('¿Está seguro que desea eliminar esta categoría?');">Eliminar esta categoría</a>
	</div>
  </body>
</html>