<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categorías</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script type="text/javascript"></script>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
    <style type="text/css">
       h1{
       	text-align: center;
       }
    </style>
  </head>
  <body>
    <div class="page-header">
    <h1>Categorías</h1>
    </div>
	<ul>
      <div class="container">
	  <table class="table">
	  <tr>
	  	   <th>Nombre de la Categoria</th>
	  	   <th>Opcion Eliminar</th>
	  </tr>
	  <?php foreach ($categories as $category): ?>
	  <form method="POST" action="/categories/deletecategory">	 
	  <input type="hidden" name="CategoryIds" value="<?=$category["id"]?>"/>
	
	  <tr>
		  <td><li><a href="category/index/<?= $category["id"] ?>"><?= $category["name"]?></a></li></td>
		  <td><input class="btn btn-danger" type="submit" name="deletecategory" value="eliminar"/></td>
	  </tr>
	  </form>
	  <?php endforeach; ?>
    	<form method="POST" action="/categories/create">
    	   <p>Ingese el Nombre de la Categoria</p>
		  <input type="text" name="newCategoryTextBox" maxlength="50"/>
		  <input class="btn btn-success" type="submit" name="createNewCategoryButton" value="Crear" />
		</form>
	  </table>
	  </div>
	</ul>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </body>
</html>
